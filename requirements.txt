mypy==0.812
pylint==2.7.1
pylint-json2html==0.3.0
pytest==6.2.4
pytest-html==3.1.1
lxml
pylint_runner==0.6.0
